/**
 * Created by Dmitry Vereykin on 7/28/2015.
 */

public class GeometryCalc {
    public static double getArea(double radius) {
        return Math.PI * radius * radius;
    }

    public static double getArea(double length, double width) {
        return length * width;
    }

    public static double getArea(double base, double height, double constant) {
        return base * height * constant;
    }
}